#!/bin/bash
# Date : (2018-10-13)
# Wine version used : 3.17
# Author : Norore
# Licence : GPLv3
# PlayOnLinux: 4.2.10
# Only For : http://www.playonlinux.com
# Notes: This script uses latest wine version. For now it is possible to install battle.net and log in. Games not tested.

[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"

# Setting the variables
PREFIX="battle.net"
WINEVERSION='3.17'
POL_System_SetArch "amd64"
TITLE="Battle.Net"
EDITOR="Blizzard Entertainment Inc."
GAME_URL="https://www.blizzard.com/fr-fr/apps/battle.net/desktop"
AUTHOR="Norore"

#Initialization
POL_GetSetupImages "http://files.playonlinux.com/resources/setups/$PREFIX/top.jpg" "http://files.playonlinux.com/resources/setups/$PREFIX/left.jpg" "$TITLE"
POL_SetupWindow_Init
POL_SetupWindow_SetID 2599
POL_Debug_Init

# Presentation
POL_SetupWindow_presentation "$TITLE" "$EDITOR" "$GAME_URL" "$AUTHOR" "$PREFIX"
POL_System_TmpCreate "$PREFIX"

cd "$POL_System_TmpDir"
curl -s --output "Battle.net-Setup.exe" 'https://eu.battle.net/download/getInstaller?os=win&installer=Battle.net-Setup.exe'

# Create Prefix
POL_Wine_SelectPrefix "$PREFIX"
POL_Wine_PrefixCreate "$WINEVERSION"

# Configuration
Set_OS "win10"

# Dependencies
# Source: https://us.battle.net/forums/en/d3/topic/16774227309
POL_Install_corefonts
POL_Install_dotnet45
POL_Install_dxfullsetup
POL_Install_gdiplus
POL_Install_gecko
POL_Install_msxml3
POL_Install_msxml4
POL_Install_msxml6
POL_Install_RegisterFonts
POL_Install_riched20
POL_Install_riched30
POL_Install_tahoma
POL_Install_tahoma2
POL_Install_vcrun2010
POL_Install_vcrun2012
POL_Install_vcrun2013

# DLL's configuration
#POL_Wine_OverrideDLL "native" "d3dx10_43" "d3dx11_43" "d3dx11_42" "d3dx9_36" "d3dx9_42" "d3dx9_43"

# Installation
POL_SetupWindow_message "$(eval_gettext 'NOTICE: Do not close $TITLE until installation completes and you are at the $TITLE login window. ')" "$TITLE"

POL_Wine "$POL_System_TmpDir/Battle.net-Setup.exe"
POL_Wine_WaitExit "$TITLE"

# Create Shortcut
POL_Shortcut "Battle.net Launcher.exe" "$TITLE"

# Cleanup
POL_System_TmpDelete

# Final message
POL_SetupWindow_message "$(eval_gettext '$TITLE is installed')" "$TITLE"

# Min memory size to run app
POL_SetupWindow_VMS "128"

# Rebooting
POL_Wine_reboot

# Exiting the  POL window
POL_SetupWindow_Close
exit 0
